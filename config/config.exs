# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :partes,
  ecto_repos: [Partes.Repo]

# Configures the endpoint
config :partes, PartesWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "epUNdwPhHflJDLbg8nhOOQ8S/Lm+QgMVU1lhc9bD8eozkW81XfZyGjC6ZSUtstbN",
  render_errors: [view: PartesWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Partes.PubSub,
  live_view: [signing_salt: "nBmBySmU"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
