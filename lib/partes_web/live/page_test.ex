defmodule PartesWeb.PageTest do
  @moduledoc """
    iex(4)>  Phoenix.LiveView.Helpers.live_patch
    live_component/2    live_component/3    live_component/4
    live_flash/2        live_patch/2        live_redirect/2
    live_render/2       live_render/3       live_title_tag/1
    live_title_tag/2    sigil_L/2


    iex(5)> Phoenix.LiveView.Router.
    fetch_live_flash/2    live/2                live/3
    live/4
    iex(5)> Phoenix.LiveView.Router.
  """

  use PartesWeb, :live_view

  import Phoenix.HTML.Tag
  import Phoenix.LiveView.Helpers, only: [live_patch: 2, live_patch: 1]


  alias Partes, as: P

  def this_live_path(opts), do: Routes.live_path(PartesWeb.Endpoint, PartesWeb.PageTest, opts)


  @page_title "Partes: sharing doc"

  @impl true
  def mount(_params, _session, socket) do
    path = "."
    tree = make_tree(path)

    tree = Map.update!(tree, ".", & %{&1|open: true})

    ui_tree = render_tree(tree)

    socket = socket
    |> assign(page_title: @page_title, path: path, tree: tree, ui_tree: ui_tree, content: nil, current_dir: ".")

    {:ok, socket}
  end

  def handle_params(params, _uri, socket) do
    IO.inspect(params, label: "PARAMS")
    # http://localhost:4000/test?content-path=./pikachu.jpg
    socket = case params["content-path"] do
      x when x in [nil, ""] ->
        IO.puts("removing body class")
        socket |> assign(content: nil, body_class: "")
      path ->
        url = Partes.Spec.spec2url(%{content_id: path, params: :original}) |> IO.inspect(label: "IMG URL: ")
        socket |> assign(content: %{url: url, type: content_type(path)}, body_class: "noscroll")
    end

    socket = case params["current_dir"] do
      x when x in [nil, ""] -> socket |> assign(current_dir: "")
      current_dir -> socket |> assign(current_dir: current_dir)
    end

    {:noreply, socket}
  end


  def content_type(content_path) do
    case Path.extname(content_path) |> String.downcase do
      ".jpg" -> :jpg
      ".mp4" -> :mp4
    end
  end


  @impl true
  # def handle_event("content-view", %{"content-path" => path}, socket) do
  #   url = Partes.Spec.spec2url(%{content_id: path, params: :original}) |> IO.inspect(label: "IMG URL: ")
  #   {:noreply, assign(socket, content: url)}
  # end

  def handle_event("content-close", _, socket) do
    {:noreply, socket |> push_patch(to: this_live_path([{"content-path", nil}]))}
  end

  def handle_event("content-next", _, socket) do
    %{assigns: %{content: %{url: content_url}, tree: tree}} = socket

    {%{name: dir_name}, %{name: content_name}} = get_neigh_content(content_url, tree, :next)

    {:noreply, socket |> push_patch(to: this_live_path([{"content-path", Path.join(dir_name, content_name)}]))}
  end

  def handle_event("content-prev", _, socket) do
    %{assigns: %{content: %{url: content_url}, tree: tree}} = socket

    {%{name: dir_name}, %{name: content_name}} = get_neigh_content(content_url, tree, :prev)

    {:noreply, socket |> push_patch(to: this_live_path([{"content-path", Path.join(dir_name, content_name)}]))}
  end

  def get_neigh_content(content_url, tree, neigh_type) do
    # should be file, and not content... file_id... the video player!!
    # what?! what is the tree in fact... id everywhere?!
    {:ok, %{content_id: content}} = Partes.Spec.url2spec(content_url)

    {dirname, filename} = {Path.dirname(content), Path.basename(content)} |> IO.inspect(label: "dir/file")

    %{items: items} = dir = tree[dirname]

    [first|_] = new_items = case neigh_type do :next -> items ; :prev -> items |> Enum.reverse() end


    new_content = case new_items |> Stream.drop_while(& &1.name != filename) |> Stream.drop(1) |> Enum.take(1) do
      [] -> first
      [x] -> x
    end

    {dir, new_content}
  end


  @impl true
  def handle_event("toggle", %{"dirname" => path}, socket) do
    %{assigns: %{tree: tree}} = socket

    new_tree = Map.update!(tree, path, & %{&1|open: not &1.open})

    {:noreply, assign(socket, tree: new_tree, ui_tree: render_tree(new_tree))}
  end

 #   @impl true
 #   def handle_event("navigate", %{"dirname" => path}, socket) do
 #     {:noreply, assign(socket, current_dir: path)}
 #   end



  def make_dir_attrs(name), do: []

  def make_tree(_path), do: Partes.Path.tree()

  def render_tree(tree), do: render_tree(tree, ".")
  def render_tree(tree, dir_path) do
    %{name: name, dirs: dirs, items: items, open: open} = tree[dir_path]

    classes = "partes-tree-dir " <> case open do true -> "opened" ; false -> "closed" end;

    content_tag :div, [{:class, classes}] do
      [
        # content_tag(:div, name, [
        #   {:class, "partes-tree-dir-name"},
        #   {"phx-click", "navigate"},
        #   {"phx-click", "toggle"},
        #   {"phx-value-dirname", dir_path}
        # ])
        live_patch(name, to: this_live_path([{:current_dir, dir_path}]))
      ] ++ case open do
        false -> []
        true ->
          Enum.concat([
            dirs |> Enum.map(& render_tree(tree, &1)),
            items |> Enum.map(& render_tree_item(&1))
          ])
        end
    end
  end

  def render_tree_item(%{dir: dir, name: name, url: url}) do
    attrs = [
      {:class, "partes-tree-item"},
      {:to, this_live_path([{"content-path", Path.join(dir, name)}])}
    ]

    live_patch attrs do
      [
        content_tag(:div, name),
        img_tag(url, class: "partes-tree-item-thumb")
      ]
    end
  end



end
