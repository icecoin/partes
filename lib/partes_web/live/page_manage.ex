defmodule PartesWeb.PageManage do
  @moduledoc """
  """

  use PartesWeb, :live_view

  import Phoenix.HTML.Tag
  import Phoenix.LiveView.Helpers


  alias Partes, as: P

  def this_live_path(opts), do: Routes.live_path(PartesWeb.Endpoint, PartesWeb.PageManage, opts)

  @page_title "Partes: management"

  @impl true
  def mount(_params, _session, socket) do

    socket = socket
    |> assign(page_title: @page_title)
    |> assign(uploaded_files: [])
    |> allow_upload(:docs, accept: ~w(.jpg .mp4), max_entries: 200) #, auto_upload: true)

    {:ok, socket}
  end

  @impl true
  def handle_params(params, _uri, socket) do
    {:noreply, socket}
  end

  @impl true
  def handle_event("validate", _params, socket) do
    {:noreply, socket}
  end

  def handle_event("save", _params, socket) do

    uploaded_files = consume_uploaded_entries(socket, :docs, fn %{path: path}=a, entry ->
        # dest = Path.join("priv/static/uploads", Path.basename(path))
        # File.cp!(path, dest)
        # Routes.static_path(socket, "/uploads/#{Path.basename(dest)}")

        # client_name: "DSC_2134.JPG",
        # client_size: 2158505,
        # client_type: "image/jpeg",

        Partes.Uploader.put(entry.client_name, File.read!(path))

        # receive do
        # after
        #   5000 -> IO.puts("yo")
        # end
      end)

    {:noreply, update(socket, :uploaded_files, &(&1 ++ uploaded_files))}
  end
end

