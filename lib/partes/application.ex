defmodule Partes.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      Partes.Repo,
      # Start the Telemetry supervisor
      PartesWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: Partes.PubSub},
      # Start the Endpoint (http/https)
      PartesWeb.Endpoint,
      # Start a worker by calling: Partes.Worker.start_link(arg)
      # {Partes.Worker, arg}
      Partes.FTP
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Partes.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    PartesWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
