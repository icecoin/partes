defmodule Partes.Repo do
  use Ecto.Repo,
    otp_app: :partes,
    adapter: Ecto.Adapters.Postgres
end
