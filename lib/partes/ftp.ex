defmodule Partes.FTP do
  use Supervisor

  def start_link(arg) do
    Supervisor.start_link(__MODULE__, arg, name: __MODULE__)
  end

  @impl true
  def init(_arg) do
    children = [
      %{
        id: :mybifrost,
        start: {:bifrost, :start_link, [Partes.FTP.Memory, [{:port, 2121}]]}
      }
    ]

    Supervisor.init(children, strategy: :one_for_one)
  end

  defmodule BitfrostHelper do
    # https://hexdocs.pm/elixir/Record.html
    require Record

    Record.defrecord(:connection_state, Record.extract(:connection_state, from_lib: "bifrost/include/bifrost.hrl"))
    Record.defrecord(:file_info, Record.extract(:file_info, from_lib: "bifrost/include/bifrost.hrl"))
  end

  defmodule FileInfo do
    require Record
    Record.defrecord(:file_info, Record.extract(:file_info, from_lib: "kernel/include/file.hrl"))
  end
end


defmodule Partes.FTP.Memory do
  require Partes.FTP.BitfrostHelper, as: BH
  require Partes.FTP.FileInfo, as: FileInfo

  @mod Partes.FTPAPI
  @root_dir "data/original"

  def root_dir(), do: Path.absname(@root_dir)
  def valid_path?(path), do: String.starts_with?(path, root_dir())
  def to_relative_path(path) do
    ["", rest] = String.split(path, root_dir(), parts: 2)
    rest
  end

  def init(state, _), do: state

  def login(state, username, password) do
    new_state = state |> put_inner_state(%{current_dir: root_dir()})

    logged = case {to_string(username), to_string(password)} |> IO.inspect(label: "LOGIN") do
      _ -> true
      # {"sim", "coin"} -> true
      #   _ -> false
    end

    {logged, new_state}
  end

  def current_directory(state) do
    get_inner_state(state).current_dir |> to_relative_path |> to_char_list
  end

  def make_directory(state, path) do
    # nothing to do will be created at end?
    case get_abs_path(state, path) do
      {:ok, new_path} -> @mod.on_make_directory(new_path) ; {:ok, state}
      _ -> {:error, :unknown}
    end
  end

  def change_directory(state, path) do
    IO.inspect(path, label: "CHANGE DIR")

    # build_new_path
    case get_abs_path(state, path) do
      {:ok, new_path} -> state |> put_inner_state(:current_dir, new_path)
      _ -> state
    end
  end

  def list_files(state, path) do
    case get_abs_path(state, path) |> IO.inspect(label: "abs_path") do
      :error -> {:error, state}
      {:ok, new_path} ->
        case File.ls(new_path) do
          {:ok, files} ->
            files
            |> IO.inspect(label: "LS")
            |> Enum.map(& regular_file_info(Path.join(new_path, &1)))
            |> Enum.reject(&is_nil/1)
            |> IO.inspect(label: "LS2")

          {:error, :enoent=reason} -> IO.inspect(reason, label: "ls failed for #{new_path}") ; []
        end
    end

  end

  def remove_directory(state, path) do
    # FIXME: remove thumbs
    case get_abs_path(state, path) do
      {:ok, new_path} ->
        if new_path == root_dir() do
          {:error, :unauthorized}
        else
          case File.rm_rf(new_path) |> IO.inspect(label: "removing dir #{new_path}") do
            {:ok, deleted} ->
              @mod.on_delete_dir(new_path, deleted)
              {:ok, state}
            {:err, :enoent} -> {:error, :not_found}
            _ -> {:error, :unknown}
          end
        end
      _ -> {:error, :unknown}
    end
  end

  def remove_file(state, path) do
    case get_abs_path(state, path) do
      {:ok, new_path} ->
        case File.rm(new_path) |> IO.inspect(label: "removing file #{new_path}") do
          :ok ->
            @mod.on_delete_file(new_path)
             {:ok, state}
          {:err, :enoent} -> {:error, :not_found}
          _ -> {:error, :unknown}
        end
      _ -> {:error, :unknown}
    end
  end


  # ftp> put test.JPG vegeta/toto.jpg
  # Creating: /home/djcoin/work/partes/data/original/vegeta/toto.jpg(filename: vegeta/toto.jpg)
  def put_file(state, filename, _mode, cb) do
    case get_abs_path(state, filename) do
      {:ok, new_path} ->
        IO.puts(["Creating: ", new_path, " (filename: ", filename, ")"])

        @mod.on_put_file(new_path, reading_from_stream(cb))

        {:ok, state}
      _ -> {:error, :unknown}
    end
  end

  def get_file(state, path) do
    case get_abs_path(state, path) do
      {:ok, new_path} ->
        case File.open(new_path) do
          {:ok, f} -> {:ok, reading_fun(state, f)}
          _ -> {:error, :cant_read}
        end
      _ -> {:error, :bad_path}
    end
  end


  # to be tested
  def file_info(state, path) do
    case get_abs_path(state, path) do
      {:ok, new_path} -> {:ok, regular_file_info(new_path)}
      _ -> {:error, :unknown}
    end
  end

  def rename_file(state, from, to) do
    case {get_abs_path(state, from), get_abs_path(state, to)} do
      {{:ok, from_p}, {:ok, to_p}} ->
        # FIXME: remove thumb
        case File.rename(from_p, to_p) |> IO.inspect(label: "renaming #{from_p} -> #{to_p}") do
          :ok -> {:ok, state}
          _ -> {:error, :rename_failure}
        end
      _ -> {:error, :bad_path}
    end
  end

  def site_command(state, _cmd_name, cmd_args) do
    {:error, :not_found}
  end

  def site_help(state) do
    {:error, :not_found}
  end

  def disconnect(state) do
    state
  end

  # helpers
  def get_inner_state(state), do: BH.connection_state(state, :module_state)
  def put_inner_state(state, inner_state), do: BH.connection_state(state, module_state: inner_state)
  def put_inner_state(state, k, v) do
    is = get_inner_state(state)
    state |> put_inner_state(Map.put(is, k, v))
  end

  def get_abs_path(_state, [?/ | _]=path) do
    new_path = root_dir() <> to_string(path) |> Path.expand
    if valid_path?(new_path), do: {:ok, new_path}, else: :error
  end

  def get_abs_path(state, path) do
    %{current_dir: dir} = get_inner_state(state)
    new_path = to_string(path) |> Path.absname(dir) |> Path.expand

    if valid_path?(new_path), do: {:ok, new_path}, else: :error
  end

  def fileinfo_to_bitfrost_fileinfo(
    path, FileInfo.file_info(type: type, mode: mode, uid: uid, gid: gid, mtime: mtime, size: size)
  ) do
    BH.file_info(
      type: case type do :regular -> :file ; :directory -> :dir end,
      name: Path.basename(path),
      mode: mode,
      uid: uid,
      gid: gid,
      size: size,
      mtime: mtime
      # module_info
    )
  end

  defp regular_file_info(path) do
    case :file.read_file_info(path) do
      {:ok, FileInfo.file_info(type: t)=file_info} when t in [:regular, :directory] ->
        fileinfo_to_bitfrost_fileinfo(path, file_info)
      _ ->
        nil
    end
  end

  def reading_from_stream(cb) do
    Stream.resource(
      fn -> cb end,
      fn cb ->
        case cb.() do
          {:ok, bytes, _bytes_count} -> {[bytes], cb}
          :done -> {:halt, cb}
        end
      end,
      fn _ -> end
    )
  end

  def reading_fun(state, fd) do
    fn nb_bytes ->
      case IO.binread(fd, nb_bytes) do
        data when is_binary(data) ->
          {:ok, data, reading_fun(state, fd)}
        _ ->
          File.close(fd)
          {:done, state}
      end
    end
  end

end


# Memo:
# behaviour_info(callbacks) ->
#     % Path :: String
#     % State Change :: {ok, State} OR {error, State}
#     % File Name :: String
#     % HelpInfo :: {Name, Description}
#     [{init, 2}, % State, PropList (options) -> State
#      {login, 3}, % State, Username, Password -> {true OR false, State}
#      {current_directory, 1}, % State -> Path
#      {make_directory, 2}, % State, Path -> State Change
#      {change_directory, 2}, % State, Path -> State Change
#      {list_files, 2}, % State, Path -> [FileInfo] OR {error, State}
#      {remove_directory, 2}, % State, Path -> State Change
#      {remove_file, 2}, % State, Path -> State Change
#      {put_file, 4}, % State, File Name, (append OR write), Fun(Byte Count) -> State Change
#      {get_file, 2}, % State, Path -> {ok, Fun(Byte Count)} OR error
#      {file_info, 2}, % State, Path -> {ok, FileInfo} OR {error, ErrorCause}
#      {rename_file, 3}, % State, From Path, To Path -> State Change
#      {site_command, 3}, % State, Command Name String, Command Args String -> State Change
#      {site_help, 1}, % State -> {ok, [HelpInfo]} OR {error, State}
#      {disconnect, 1}]; % State -> State Change
#
#
# -record(connection_state,
#         {
#           authenticated_state = unauthenticated,
#           user_name,
#           data_port = undefined,
#           pasv_listen = undefined,
#           ip_address = undefined,
#           rnfr = undefined,
#           module,
#           module_state,
#           ssl_allowed = false,
#           ssl_cert = undefined,
#           ssl_key = undefined,
#           ssl_ca_cert = undefined,
#           protection_mode = clear,
#           pb_size = 0,
#           control_socket = undefined,
#           ssl_socket = undefined,
#           utf8 = false
#          }).
# 
# -record(file_info,
#         {
#           type, % dir or file
#           name,
#           mode,
#           uid,
#           gid,
#           size,
#           mtime,
#           module_info
#          }).
