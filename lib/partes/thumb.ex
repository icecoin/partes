
# TODO: path is not specific enough
defmodule Partes.Spec do
  @root_fs_dir "data"
  @root_url_dir "public"

  def get_root_url_dir(), do: @root_url_dir
  def get_root_fs_dir(), do: @root_fs_dir

  def full_path_to_rel_path(full_path) do
    fs_dir_absolute = Path.absname(@root_fs_dir)
    ["", "/" <> rel_path] = String.split(full_path, fs_dir_absolute, parts: 2)
    rel_path
  end

  def full_path_original_to_content_id(full_path) do
    "original/" <> content_id = full_path_to_rel_path(full_path)
    content_id
  end

  @moduledoc """
  1. extract url to width / height
  2. check if we got this in cache


  Propriété:
  - toujours précisé l'content_id original (url ou spec)
    ~ si l'content_id correspond à un .mp4 qui doit changé de format, ajoute-on une extension ?
  - retrouver à partir d'une url/spec -> l'original
  - retrouver à partir d'un path..?

  """

  # storing in FS
  # / resize / filetype / ...
  # / resize / file.filetype
  #
  # always append format at end! => this won't modify the directory structure

  # def original2path() do
  #   Path.join([@root_fs_dir, "#{w}x#{h}", path)
  # end

  def spec2path(%{content_id: path, params: :original}) do
    Path.join([@root_fs_dir, "original", path])
  end

  # encode an original path and params to a filesystem path (storing purpose)
  def spec2path(%{content_id: path, params: %{width: w, height: h, ext: ext}}) do
    Path.join([@root_fs_dir, "#{w}x#{h}", path <> "#{ext}"])
  end

  def specdir2dirpath(%{content_id: dir_path, params: %{width: w, height: h, ext: _ext}}) do
    Path.join([@root_fs_dir, "#{w}x#{h}", dir_path])
  end

  # encode an original path and params to an url path
  def spec2url(%{content_id: path, params: :original}) do
    URI.encode("#{@root_url_dir}/#{path}")
  end

  # encode an original path and params to an url path
  def spec2url(%{content_id: path, params: %{width: w, height: h, ext: ext}}=data) do
    URI.encode("#{@root_url_dir}/#{path}#{ext}?width=#{w}&height=#{h}")
  end

  @default_ext ".jpg"
  @accepted_ext [".jpg"]
  @accepted_wh [{200, 200}]
  @accepted_op for ext <- @accepted_ext, wh <- @accepted_wh, do: {ext, wh}

  # "public/mydir/pikachu.jpg?width=3&height=5
  # watch out for "/" prefix

  def accepted_ext(), do: @accepted_ext
  def accepted_wh(), do: @accepted_wh
  def accepted_op(), do: @accepted_op

  # URL are cured, you can't request what you want for what they accept as format and dimensions
  # otherwise specify your own format
  def url2spec(@root_url_dir <> "/" <> relative_url) do
    %{path: path, query: q} = URI.parse(relative_url)

    params = case q != nil && URI.decode_query(q) do
      %{"width" => w, "height" => h} ->
          {content_id, ext} = String.split_at(path, -4)

          case {ext, Integer.parse(w), Integer.parse(h)} do
            {ext, {w, ""}, {h, ""}} when ext in @accepted_ext and {w, h} in @accepted_wh  ->
              {:ok, %{content_id: content_id, params: %{width: w, height: h, ext: ext}}}
            _ ->
              {:error, :bad_url_params}
          end


       %{}=params when map_size(params) == 0 -> {:ok, %{content_id: path, params: :original}}
       false -> {:ok, %{content_id: path, params: :original}}
    end
  end

  def url2spec(_), do: {:error, :bad_url}

  def url2path(url) do
    case url2spec(url) do
      {:ok, spec} -> {:ok, spec2path(spec)}
      {:error, _} = e -> e
    end
  end

  def add_filetype(%{ext: ext}=fspec) do
    filetype = case ext do
      ".jpg" -> :img
      ".mp4" -> :vid
    end
    %{fspec|filetype: filetype}
  end

  def path2filespec(path) do
    %{
      rel_path: path,
      name: Path.basename(path),
      ext: Path.extname(path) |> String.downcase,
      abs_name: Path.absname(path),
      bin: :pending,
      filetype: :unknown,
    }
    |> add_filetype()
  end


end


defmodule Partes.Thumb do
  alias Imageflow.Graph, as: G

  @doc """
  Take a file spec, what's asked, return an operation to perform on it
  """

  def params2op(%{filetype: :vid}, %{width: _, height: _}=params), do: {:vid_thumb, params}
  def params2op(%{filetype: :img}, %{width: _, height: _}=params), do: {:img_resize, params}


  def process({:img_resize, %{width: width, height: height}}, orig_path, output_path) do
    IO.puts("#{orig_path} => #{output_path}")

    File.mkdir_p!(Path.dirname(output_path))

    res =
    G.new()
    |> G.decode_file(orig_path)
    # operation
    |> G.constrain(width, height)
    |> G.encode_to_file(output_path)
    |> G.run()

    :ok
  end

  def process({:vid_thumb, %{width: width, height: height}}, orig_path, output_path) do
    File.mkdir_p!(Path.dirname(output_path))

    IO.puts("#{orig_path} => #{output_path}")

    # pwoblem
    Thumbnex.create_thumbnail(orig_path, output_path, max_width: width, max_height: height)

    :ok
  end


end


defmodule Partes.Get do

  def get(%{content_id: _, params: _}=spec) do
    # WARNING: returns type differ, bin or output_path
    case Partes.Cache.fetch(spec) do
      # returns a bin, or a stream, or a fpath, ...?
      {:ok, bin} -> {:ok, bin}
      {:error, _} -> Partes.Cache.compute_and_store(spec)
    end
  end

  def get(uri) when is_binary(uri) do
    case Partes.Spec.url2spec(uri) do
      {:ok, spec} -> get(spec)
      {:error, _}=e -> e
    end
  end

end


defmodule Partes.Uploader do
  @upload_dir "./uploads"

  # :filelib.wildcard('uploads/vegeta/**') |> Enum.map(& to_string(&1) |> String.trim_leading("uploads/"))
  #
  def load_dir(dir) do
    # check for ./
    :filelib.fold_files(String.to_charlist(dir), '.*', true, & ["#{&1}"|&2] , [])
  end

  def put(@upload_dir <> path = full_path), do: put(path, File.read!(full_path))

  def put(id, bin) do
    output_path = Partes.Spec.spec2path(%{content_id: id, params: :original})

    IO.puts("#{id} => #{output_path}")
    File.mkdir_p!(Path.dirname(output_path))

    File.write!(output_path, bin)
  end

end


defmodule Partes.Cache do
  @doc """
    Given a spec:
    - tries to find the original file, saved w/ the Upload
    - ifnot return error
    - if exists
  """
  def cached_path(%{content_id: _, params: _}=spec), do: Partes.Spec.spec2path(spec)
  def fetch(%{content_id: _, params: _}=spec), do: spec |> cached_path |> File.read

  def compute_and_store(%{content_id: _, params: :original}=spec) do
    spec |> cached_path |> File.read
  end

  # compute and store
  def compute_and_store(%{content_id: content_id, params: %{width: _w, height: _h, ext: _ext}=params}=requested_spec) do
    # check existence + could check ACL etc.

    original_spec = %{requested_spec|params: :original}
    fs_path = Partes.Spec.spec2path(original_spec) |> IO.inspect(label: "checking original path")

    case File.stat(fs_path) do
      {:ok, %{type: :regular, access: a}} when a in [:read, :read_write] ->
        operation = Partes.Spec.path2filespec(fs_path) |> Partes.Thumb.params2op(params)

        # different extension
        output_path = cached_path(requested_spec)

        # returns ? {:ok, binary} ? {:ok, file}
        :ok = Partes.Thumb.process(operation, fs_path, output_path)

        {:ok, output_path}

      {:error, _}=e -> e
    end
  end

end


defmodule Partes.Path do

  @doc """
  dirs: list of directory

  %{
    "." => %{
      dirs: ["myvids", "vegeta"],
      items: [
        %{
          dir: ".",
          name: "pikachu.jpg",
          url: "public/data/original/pikachu.jpg.jpg?width=200&height=200"
        }
      ],
      name: ".",
      open: false
    },
    "myvids" => %{
      dirs: [],
      items: [
        %{
          dir: "myvids",
          name: "MOV_1245.mp4",
          url: "public/data/original/myvids/MOV_1245.mp4.jpg?width=200&height=200"
        }
      ],
      name: "myvids",
      open: false
    }
  }

  """
  def tree(starting_dir \\ "data/original") do
    cb = fn filepath, acc ->
      {_, path} = to_string(filepath) |> String.split_at(String.length(starting_dir) + 1)
      dir = Path.dirname(path)
      filename = Path.basename(path)

      file = %{
        name: filename,
        dir: dir,
        url: content_url(path)
      }

      Map.update(acc, dir, [file], & [file| &1])
    end

    all_dirs = :filelib.fold_files(String.to_charlist(starting_dir), '.*', true, cb, %{})
    dir_struct = Map.keys(all_dirs) |> Enum.reject(& &1 == ".") |> Enum.group_by(& Path.dirname/1) # parent to child

    all_dirs
    |> Map.new(fn {dir, v} ->
      {dir, %{
        name: dir,
        items: Enum.reverse(v),
        dirs: Map.get(dir_struct, dir, []),
        open: false
      }}
    end)
  end

  @default_params %{width: 200, height: 200, ext: ".jpg"}
  def content_url(path), do:
    Partes.Spec.spec2url(%{content_id: path, params: @default_params})

  def ls(d) do
    path = Partes.Spec.get_root_fs_dir() |> Path.join("original") |> Path.join(d)

    case File.ls(path) do
      {:ok, ls} ->
        ls |> Enum.map(fn f ->
          full_path = Path.join(path, f)
          cond do
            File.regular?(full_path) -> {f, :file}
            File.dir?(full_path) -> {f, :dir}
            true -> nil
          end
        end)
        |> Enum.reject(&is_nil(&1))
      _ -> []
    end
    |> Enum.map(fn
      {name, :dir = type} -> %{name: name, type: type, url: nil}
      {name, :file = type} -> %{
          name: name,
          type: type,
          url: Partes.Spec.spec2url(%{content_id: Path.join(d, name), params: @default_params})
        }
    end)
  end


end

defmodule Partes.FTPAPI do
  def on_delete_dir(root_path, _deleted) do
    root_path |> Partes.Spec.full_path_original_to_content_id |> delete_dir
  end

  def delete_dir(rel_path) do
    IO.inspect(rel_path, label: "FTPAPI, delete dir #{rel_path}")
    base = %{content_id: rel_path, params: nil}

    successful_deletes = Partes.Spec.accepted_op |> Enum.flat_map(fn {ext, {width, height}} ->
      op_path = Partes.Spec.specdir2dirpath(%{base|params: %{ext: ext, width: width, height: height}})
                |> IO.inspect(label: "Maybe deleting dir")

      case File.rm_rf(op_path) do
        :ok -> [op_path]
        _ -> []
      end
    end)
  end

  def on_delete_file(full_path), do:
    on_delete_file_rel(full_path |> Partes.Spec.full_path_original_to_content_id)

  def on_delete_file_rel(rel_path) do
    IO.inspect(rel_path, label: "delete file")
    base = %{content_id: rel_path, params: nil}

    successful_deletes = Partes.Spec.accepted_op |> Enum.flat_map(fn {ext, {width, height}} ->
      op_path = Partes.Spec.spec2path(%{base|params: %{ext: ext, width: width, height: height}})
       |> IO.inspect(label: "TESTING path")

      case File.rm(op_path) do
        :ok -> [op_path]
        _ -> []
      end
    end)

    IO.inspect(successful_deletes, label: "Delete for #{rel_path}")
  end

  def on_put_file(abs_path, stream) do
    # FIXME, should be handled by Uploader?
    File.mkdir_p!(Path.dirname(abs_path))
    stream |> Stream.into(File.stream!(abs_path)) |> Stream.run
  end

  def on_make_directory(abs_path) do
    # check everything is ok
    case abs_path |> Partes.Spec.full_path_original_to_content_id do
      x when is_binary(x) -> :ok
    end
    abs_path |> IO.inspect(label: "Making dir")
    File.mkdir_p!(abs_path)
  end
end


defmodule Partes.Thumb.Test do
  import Partes.Thumb

  @upload_dir "./uploads"
  def upload_path(path), do: @upload_dir <> "/" <> path

  @base_img "pikachu.jpg"
  @base_vid "myvids/MOV_1245.mp4"

  def t_upload() do
    Partes.Uploader.put(upload_path(@base_img))

    url1 = Partes.Spec.get_root_url_dir() <> "/" <> @base_img |> IO.inspect
    IO.inspect(url1, label: "url1")

    {:ok, spec} = Partes.Spec.url2spec(url1)
    IO.inspect(spec, label: "url2spec")

    url2 = Partes.Spec.spec2url(spec)
    IO.inspect(url2, label: "url2")

    path = Partes.Spec.spec2path(spec)
    IO.inspect(path, label: "path")

    Partes.Get.get(url1)
  end

  def t_resize() do
    url = Partes.Spec.get_root_url_dir() <> "/" <> @base_img <> ".jpg"

    Partes.Get.get(url <> "?width=200&height=200")
  end

  def t_thumb() do
    Partes.Uploader.put(upload_path(@base_vid))

    # Partes.Get.get(%{content_id: @base_vid, params: :original})
    Partes.Get.get(%{content_id: @base_vid, params: %{width: 200, height: 200, ext: ".jpg"}})
  end

end

